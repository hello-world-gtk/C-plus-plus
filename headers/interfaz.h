#pragma once

#include <gtkmm/window.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/entry.h>
#include <gtkmm/box.h>

class Interfaz : public Gtk::Window {
    public:
        Interfaz();
        ~Interfaz();
    protected:
        void onToggledEditable();
        void onToggledVisible();
        void onClickedButton();
        void onChangedEntry();
    private:
        Gtk::Label label;
        Gtk::CheckButton editable, visible;
        Gtk::Button buttonClose;
        Gtk::Entry entry;
        Gtk::Box container, checkButtons;
};