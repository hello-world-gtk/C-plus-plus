#include "headers/interfaz.h"

Interfaz::Interfaz() : label("Hola Mundo"), editable("Editable"), visible("Visible"), buttonClose("Cerrar"), container(Gtk::ORIENTATION_VERTICAL) {
    set_title("Prueba Interfaz");
    container.add(label);
    label.set_margin_bottom(10);
    container.add(entry);
    entry.set_margin_bottom(10);
    container.add(checkButtons);
    checkButtons.pack_start(editable);
    checkButtons.pack_end(visible);
    checkButtons.set_border_width(10);
    container.pack_end(buttonClose);

    editable.set_active();
    visible.set_active();

    //Logica
    entry.signal_changed().connect(sigc::mem_fun(*this, &Interfaz::onChangedEntry));
    editable.signal_toggled().connect(sigc::mem_fun(*this, &Interfaz::onToggledEditable));
    visible.signal_toggled().connect(sigc::mem_fun(*this, &Interfaz::onToggledVisible));
    buttonClose.signal_clicked().connect(sigc::mem_fun(*this, &Interfaz::onClickedButton));

    //Mostrar
    add(container);
    show_all();
}

Interfaz::~Interfaz() {}

void Interfaz::onChangedEntry() {
    label.set_text((entry.get_text() == "") ? "Hola Mundo" : ("Hola " + entry.get_text()));
}

void Interfaz::onToggledEditable() {
    entry.set_editable(editable.get_active());
}

void Interfaz::onToggledVisible() {
    entry.set_visibility(visible.get_active());
}

void Interfaz::onClickedButton() {
    hide();
}