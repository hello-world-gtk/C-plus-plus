#include "headers/interfaz.h"

#include <gtkmm/application.h>

int main(int argc, char *argv[]) {
    auto app = Gtk::Application::create(argc, argv, "com.interfaz.org");
    Interfaz interfaz;
    return app->run(interfaz);
}